#p = [0.2, 0.2, 0.2, 0.2, 0.2]
p = [0.0, 1.0, 0.0, 0.0, 0.0]
world = ['green', 'red', 'red', 'green', 'green']
measurements = ['red', 'green']
motion = [1, 1]

pCorrect = 0.6
pWrong = 0.2
pOvershoot = 0.1
pUndershoot = 0.1
pExactshoot = 0.8



def sense(p, measurement):
	q=[]
	for i in range(len(p)):
		h = (world[i] == measurement)
		q.append(p[i] * pCorrect * h + p[i] * pWrong * (1-h))

	qSum = sum(q)
	for i in range(len(p)):
		q[i] = q[i] / qSum

	return q


def move(p, U): # +ve value of U implies motion to right side
	q=[]
	for i in range(len(p)):
		due_to_exactshoot 	= p[(i-U)%len(p)] * pExactshoot
		due_to_overshoot 	= p[(i-U-1)%len(p)] * pOvershoot
		due_to_undershoot 	= p[(i-U+1)%len(p)] * pUndershoot
		q.append(due_to_exactshoot + due_to_overshoot + due_to_undershoot)

	return q



for i in range(len(measurements)):
	p = sense(p, measurements[i])
	p = move(p,motion[i])
	print p
